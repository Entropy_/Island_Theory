using DelimitedFiles


#The following line is a bad idea, but fun to entertain. Loading the whole dataset
#filled up six and a half gigs of ram in about thirty seconds.
#fourkilohertz = readdlm("data/H-H1_GWOSC_4KHZ_R1-1126257415-4096.txt")

#Making this idea usable is of course, going to take a jig or two.

#My first thought is to first load it on an SSD, to cut down on access times.
#Second thought is to chop it into chunks. Write a small go or python program
#(Or hell, even a bash script) to pipe the data into a cluster. The cluster
#can be processed on this computer, OR! I create a small supercomputer cluster.
#ha, super. More like a junker's warp drive.<--bad idea $$$$$


#Anyways, chop it into chunks, get some points, paint points, dump chunk,
#eat next chunk, paint points, repeat.

#Some more notes. I'll have to figure out just how many points are represented here.
#If we're talking about making this a flat plot and running boring old transforms
#on things we'll have a hard time getting it on screen and not mangling the data
#beyond repair.
#
#Something I've always wanted to do is a VR type visualization. Bend spacetime with
#an impartial observation point. Wouldn't be seizure friendly, ever probably.
#But....if I make it so it's basically a ball on a screen wiggling ever so slightly
#in size...It would fall in line with the idea of watching the dot on an interferometer.
#
#Second idea, make it relate to the colour of a ball. Hell it'll make for a good screensaver.
#
#So, four balls, two for each observatory, each oscillating in colour at a quarter cat speed
#Hell, being able to tweak the speed will make this much more robust. When all four balls
#are the same colour, we'll have convergence. Later on as the program grows, getting common
#happenstances and shared experiences to trigger a chirp or something would be awesome. No one
#wants to be taking a whiz when the universe calls.
#
#Another filter option is to have them NOT change colour when the values are different by an
#established deviation. Then when they converge, the change will be drastic. This can be an
#entry-level usage. Have the balls chirp and wobble when convergence happens. Load from disk,
#play at a quarter the speed of cat-ing. We have a plan
