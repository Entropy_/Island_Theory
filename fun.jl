mutable struct HIRES
  Date::Float64
  Velocity::Float64
  Uncertainty::Float64
  S_value::Float64
  Halpha::Float64
  medianphotons::Float64
  exposure::Float64

end
