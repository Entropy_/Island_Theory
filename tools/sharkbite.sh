#!/bin/bash
linetotal=`wc -l < $1`
#use this to get the number of lines in the file

#use the line number to input into the for loop
echo "Bash version ${BASH_VERSION}..."
echo "Welcome to SharkBite!"
mkdir chum
echo ""
chunknum=1
for (( LINES=1; LINES<=$linetotal; LINES += 100000 ))
do
    echo -n "$LINES lines bitten out of $linetotal"
    tput rc
    linenum=$LINES
    endlinenum=$(( $LINES+99999 ))'p'
    sed -n $linenum,$endlinenum $1 >> chum/chunk$chunknum.txt
    chunknum=$(( $chunknum+1 ))
done
echo
