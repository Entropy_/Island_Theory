cd ~/Code/Island_Theory/
gnome-terminal --window --title='Emarcs' --geometry 212x44+0+1085 -- bash --rcfile ~/Code/Island_Theory/env/.emarc
gnome-terminal --window --title='Jular' --geometry 212x8+0+2000 -- bash --rcfile ~/Code/Island_Theory/env/.juliarc
gnome-terminal --window --title='Tarps' --geometry 78x54+1050+1 -- bash --rcfile ~/Code/Island_Theory/env/.toprc
gnome-terminal --title='Atarps' --window --geometry 126x54-0+0 -- bash --rcfile ~/Code/Island_Theory/env/.atoprc
