include("fun.jl")

using DelimitedFiles
let
rawdata = readdlm("data/testing.txt")

data = Any[]
for i in range(0, stop=5, length=5)
    #create a blank HIRES type in each slot
    push!(data, HIRES(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0) )
end
print(fieldnames(HIRES))
row = 1
for dataindex in eachindex(rawdata)
    if dataindex < 5
        data[row].Date = rawdata[dataindex]
    end
    if 5 <= dataindex < 10
        data[row].Velocity = rawdata[dataindex]
    end
    if 10 <= dataindex < 15
        data[row].Uncertainty = rawdata[dataindex]
    end
    if 15 <= dataindex < 20
        data[row].S_value = rawdata[dataindex]
    end
    if 20 <= dataindex < 25
        data[row].Halpha = rawdata[dataindex]
    end
    if 25 <= dataindex < 30
        data[row].medianphotons = rawdata[dataindex]
    end
    if 30 <= dataindex < 35
        data[row].exposure = rawdata[dataindex]
    end
    row = row + 1
    if row > 5
        row = 1
    end
end

print(data)
end
