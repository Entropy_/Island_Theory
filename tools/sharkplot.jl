using DelimitedFiles
using Plots
let
    rawdata = Any
    cd("$(pwd())/../data/LIGO/chum")
    dir = readdir(pwd())
    for i in range(1, stop=2)
        rawdata = readdlm(dir[i])
    end
    plot(rawdata, linewidth=2,title="don't jump mr shark", xlab="Time", ylab="Movement", ylim=(-0.000000000000000001,0.000000000000000001))
    
end
