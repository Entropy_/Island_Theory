"""Sieve of Eratosthenes function docstring"""
function es(n::Int) #Accepts one integer argument
    isprime = trues(n) #N-element vector of true-s
    isprime[1] = false #1 is not a prime
    for i in 2:isqrt(n) #loop integers less or equal than sqrt(n)
        if isprime[i] # Conditional evaluation
            for j in i^2:i:n #sequence with step i
                isprime[j] = false
            end
        end
    end
    return filter(x -> isprime[x], 1:n) #filter using an anonymous function
end

println(es(100)) #Print all primes less or equal than 100
@time length(es(10^6))
